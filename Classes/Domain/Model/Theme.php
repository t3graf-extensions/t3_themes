<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/t3_themes.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\T3Themes\Domain\Model;

/**
 * This file is part of the "T3 Theme Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Development-Team [T3graf media-agentur] <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * Theme
 */
class Theme extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
}
