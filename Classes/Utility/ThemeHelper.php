<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/t3_themes.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\T3Themes\Utility;

/*
 * This file is part of TYPO3 CMS-based extension "t3_themes" by T3graf media-agentur UG.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 */

use TYPO3\CMS\Core\Configuration\Loader\YamlFileLoader;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Package\Exception\UnknownPackageException;
use TYPO3\CMS\Core\Package\PackageInterface;
use TYPO3\CMS\Core\Package\PackageManager;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;

/**
 * Helper to render a dynamic selection of available site extensions
 * and to fetch a theme for certain page
 */
class ThemeHelper
{

    /**
     * @var PackageManager
     */
    protected $themeManager;

    /**
     * @var SiteFinder
     */
    protected $siteFinder;

    /**
     * @var PageRepository
     */
    protected PageRepository $pageRepository;

    protected int $id;

    public function __construct(PackageManager $themeManager, SiteFinder $siteFinder, PageRepository $pageRepository)
    {
        $this->themeManager = $themeManager;
        $this->siteFinder = $siteFinder;
        $this->pageRepository = $pageRepository;
    }

    public function getSiteThemePackage(int $pageId): ?PackageInterface
    {
        try {
            $site = $this->siteFinder->getSiteByRootPageId($pageId);
            $configuration = $site->getConfiguration();
            if (!isset($configuration['activeTheme'])) {
                return null;
            }
            $themeKey = (string)$configuration['activeTheme'];
            return $this->themeManager->getPackage($themeKey);
        } catch (SiteNotFoundException $e) {
            return null;
        } catch (UnknownPackageException $e) {
            return null;
        }
    }

    /**
     * @throws SiteNotFoundException
     */
    public function getSiteConfiguration(int $pageId): array
    {
        $siteConf = [];
        $activeTheme = $this->getSiteThemePackage((int)$pageId);
        if ($activeTheme !== null) {
            $site = $this->siteFinder->getSiteByRootPageId($pageId);
            $siteConf = $site->getConfiguration();
        }
        return $siteConf;
    }
    /**
     * @return string[]
     */
    /* public function resolveStoragePaths(): array
     {
         if (Environment::isComposerMode()) {
             //$storagePaths = $this->resolveComposerStoragePaths();
         } else {
             $storagePaths = [Environment::getExtensionsPath()];
         }

         return array_map(
             static function (string $storagePath) {
                 return rtrim($storagePath, '/') . '/';
             },
             $storagePaths
         );
     }*/

    /**
     * @return string[]
     */
    public function findAllThemeExtensions(): array
    {
        $extensions = [];
        $storagePath = Environment::getExtensionsPath();

        return array_merge($extensions, $this->findAllInDirectory($storagePath));
    }

    /**
     * @throws SiteNotFoundException
     */
    protected function findAllInDirectory(string $storagePath): array
    {
        $result = [];
        $extensionDirectoryHandle = opendir($storagePath);
        while (false !== ($singleExtensionDirectory = readdir($extensionDirectoryHandle))) {
            if ($singleExtensionDirectory[0] === '.') {
                continue;
            }
            $themeConfiguration = $this->getThemeConfiguration($singleExtensionDirectory, $storagePath);
            $is_installed = ExtensionManagementUtility::isLoaded($singleExtensionDirectory);
            if ($themeConfiguration !== null && $is_installed) {
                $result[$singleExtensionDirectory] ['isInstalled'] = $is_installed;
                $result[$singleExtensionDirectory] = array_merge(
                    $result[$singleExtensionDirectory],
                    $themeConfiguration
                );
            }
        }
        closedir($extensionDirectoryHandle);

        return self::sortActiveThemeFirst($result, $this->getActiveThemeKey($this->getRootPageId()));
    }

    /**
     * Reads the stored configuration (i.e. the extension model etc.).
     *
     * @param string $extensionKey
     * @param string|null $storagePath
     * @return array|null
     */
    public function getThemeConfiguration(string $extensionKey, ?string $storagePath = null): ?array
    {
        $yamlFile = $storagePath . '/' . $extensionKey . '/' . 'ExtensionBuilder.json';
        $themeConfigurationYaml = self::getThemeYaml($extensionKey, $storagePath);
        if ($themeConfigurationYaml) {
            return $themeConfigurationYaml;
        }
        return null;
    }

    public static function getThemeYaml(string $extensionKey, ?string $storagePath = null): ?array
    {
        $yaml = GeneralUtility::makeInstance(YamlFileLoader::class);

        $yamlFile = $storagePath . '/' . $extensionKey . '/' . 'Theme.yaml';

        if (file_exists($yamlFile)) {
            return $yaml->load($yamlFile);
        }

        return null;
    }

    /**
     * Sort the themes so that the active theme is first .
     *
     * @param array $availableThemes
     * @return array|null
     */
    public static function sortActiveThemeFirst(array $availableThemes, ?string $activeTheme): ?array
    {
        $sortResult[$activeTheme] = $availableThemes[$activeTheme];
        unset($availableThemes[$activeTheme]);

        return array_merge($sortResult, $availableThemes);
    }

    private function resolveComposerStoragePaths(): void
    {
    }

    public function getPageUid(): int
    {
        $pageUid = (GeneralUtility::_GP('id') > 0) ? GeneralUtility::_GP('id') : (int)$GLOBALS['_GET']['id'];
        return (int)$pageUid;
    }

    public function getRootLine(): array
    {
        $this->id = (int)GeneralUtility::_GP('id');
        $pageUid = $this->getPageUid();
        if ($pageUid) {
            $rootlineUtility = GeneralUtility::makeInstance(RootlineUtility::class, $this->id);
            $rootline = $rootlineUtility->get();
        }
        return $rootline;
    }
    public function getRootPageId(): ?int
    {
        $pageUid = $this->getPageUid();
        if ($pageUid) {
            $rootline = $this->getRootLine();
            $rootPageId = $rootline[0]['uid'];
        }
        return $rootPageId;
    }

    /**
     * @throws SiteNotFoundException
     */
    public function getActiveThemeKey(?int $rootPageId): ?string
    {
        $siteConf = $this->getSiteConfiguration((int)$rootPageId);
        // TODO Check if null
        return $siteConf['activeTheme'];
    }

    public function getAllRootSites(): array
    {
        //$configurationRepository = $this->configurationRepository;
        $pageRepository = $this->pageRepository;
        $sites = array_map(
            static function (Site $site) use ($pageRepository) {
                $page = $pageRepository->getPage($site->getRootPageId());
                $title = trim($page['title'] ?? $site->getIdentifier());
                $title = $title === '' ? $page['subtitle'] : $title;
                return [
                    'rootPage' => $page['uid'],
                    'rootPageTitle' => $title
                ];
            },
            GeneralUtility::makeInstance(SiteFinder::class)->getAllSites()
        );
        return array_filter($sites, static fn (array $site): bool => $site !== []);
    }
}
