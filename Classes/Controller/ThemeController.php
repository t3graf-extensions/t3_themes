<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/t3_themes.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\T3Themes\Controller;

/**
 * This file is part of the "T3 Theme Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Development-Team [T3graf media-agentur] <development@t3graf-media.de>, T3graf media-agentur UG
 */

use Psr\Http\Message\ResponseInterface;
use T3graf\T3Themes\Utility\ThemeHelper;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * ThemeController
 */
class ThemeController extends ActionController
{
    /**
     * @var ModuleTemplateFactory
     */
    protected ModuleTemplateFactory $moduleTemplateFactory;

    /**
     * @var ThemeHelper
     */
    protected ThemeHelper $themeHelper;

    public function __construct(ThemeHelper $themeHelper, ModuleTemplateFactory $moduleTemplateFactory)
    {
        $this->moduleTemplateFactory = $moduleTemplateFactory;
        $this->themeHelper = $themeHelper;
    }

    /**
     * themeRepository
     *
     * @var \T3graf\T3Themes\Domain\Repository\ThemeRepository
     */
    //protected $themeRepository = null;

    /**
     * @param \T3graf\T3Themes\Domain\Repository\ThemeRepository $ThemeRepository
     */
    /*public function injectThemeRepository(\T3graf\T3Themes\Domain\Repository\ThemeRepository $themeRepository)
    {
        $this->themeRepository = $themeRepository;
    }*/

    protected function initializeAction()
    {
        //$this->id = (int)GeneralUtility::_GP('id');
    }

    /**
     * action listThemes
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    public function listThemesAction(): ResponseInterface
    {

        //$pageUid = $this->themeHelper->getPageUid();
        $pageUid = (GeneralUtility::_GP('id') > 0) ? GeneralUtility::_GP('id') : (int)$GLOBALS['_GET']['id'];
        $rootPages = $this->themeHelper->getAllRootSites();
        //\TYPO3\CMS\Core\Utility\DebugUtility::debug($pageUid, 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
        // \TYPO3\CMS\Core\Utility\DebugUtility::debug($rootPages, 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);

        if ($pageUid) {
            $rootPageId = $this->themeHelper->getRootPageId();
            $rootline = $this->themeHelper->getRootLine();
            $siteConf = $this->themeHelper->getSiteConfiguration((int)$rootPageId);
        }
        // LIST all installed themes
        $availablethemes = $this->themeHelper->findAllThemeExtensions();
        \TYPO3\CMS\Core\Utility\DebugUtility::debug($availablethemes, 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);

        $this->view->assignMultiple([
            'rootPage' => $rootPageId,
            'rootPages' => $rootPages,
            'currentPage' =>  $rootline[0],
            'activeTheme' => $siteConf['activeTheme'],
            'availableThemes' => $availablethemes,
        ]);
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->addCssFile('EXT:t3_themes/Resources/Public/Css/Style.css', 'stylesheet', 'all', '', true);
        // $moduleTemplate = $this->moduleTemplateFactory->create($this->request);
        // Adding title, menus, buttons, etc. using $moduleTemplate ...

        // $moduleTemplate->setContent($this->view->render());
        return $this->htmlResponse();
    }

    /**
     * action activateTheme
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function activateThemeAction(): ResponseInterface
    {
        return $this->htmlResponse();
    }

    /**
     * action themeDetails
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function themeDetailsAction(): ResponseInterface
    {
        return $this->htmlResponse();
    }
}
