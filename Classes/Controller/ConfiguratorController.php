<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/t3_themes.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\T3Themes\Controller;

use Psr\Http\Message\ResponseInterface;
use T3graf\T3Themes\Domain\Repository\ConfiguratorRepository;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * ConfiguratorController
 */
class ConfiguratorController extends ActionController
{
    protected ModuleTemplateFactory $moduleTemplateFactory;
    protected ConfiguratorRepository $configuratorRepository;
    protected PageRepository $pageRepository;
    protected ?int $pageUid;
    protected ?int $templateUid;
    protected ?array $configurator;

    public function __construct(
        ModuleTemplateFactory $moduleTemplateFactory,
        ConfiguratorRepository $configuratorRepository,
        PageRepository $pageRepository
    ) {
        $this->moduleTemplateFactory = $moduleTemplateFactory;
        $this->configuratorRepository = $configuratorRepository;
        $this->pageRepository = $pageRepository;
    }

    public function initializeAction(): void
    {
        parent::initializeAction();
        //$databaseService = GeneralUtility::makeInstance(DatabaseService::class);
        $this->pageUid = (int)($this->request->getQueryParams()['id'] ?? 0);
        //$this->templateUid = (int)$databaseService->getField('sys_template', 'uid', ['pid' => $this->pageUid]);
        //$this->configurator = $databaseService->getRecord('tx_easyconf_configurator', ['pid' => $this->pageUid]);
        /*if ($this->pageUid > 0 && $this->templateUid > 0 && $this->configurator === null) {
            $this->configurator = $databaseService->addRecord(
                'tx_easyconf_configurator',
                ['pid' => $this->pageUid],
                [Connection::PARAM_INT]
            );
        }*/
    }

    public function infoAction(): ResponseInterface
    {
        $this->view->assignMultiple([
            'pageUid' => $this->pageUid,
            'templateUid' => $this->templateUid,
            'queryParams' => $this->request->getQueryParams(),
            'sites' => $this->getSitesData(),
        ]);
        $moduleTemplate = $this->moduleTemplateFactory->create($this->request);
        // Adding title, menus, buttons, etc. using $moduleTemplate ...
        $moduleTemplate->setContent($this->view->render());
        return $this->htmlResponse($moduleTemplate->renderContent());
    }

    /**
     * action editConf
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function editConfAction() : ResponseInterface
    {
        return $this->htmlResponse();
    }

    protected function getSitesData(): array
    {
        $configuratorRepository = $this->configuratorRepository;
        $pageRepository = $this->pageRepository;
        $sites = array_map(
            static function (Site $site) use ($configuratorRepository, $pageRepository) {
                $configurator = $configuratorRepository->getFirstByPid($site->getRootPageId());
                if (($configuratorUid = (int)($configurator['uid'] ?? 0)) === 0) {
                    return [];
                }
                $page = $pageRepository->getPage($site->getRootPageId());
                $title = trim($page['title'] ?? $site->getIdentifier());
                $title = $title === '' ? $page['subtitle'] : $title;
                return [
                    'configuratorUid' => $configuratorUid,
                    'rootPageTitle' => $title
                ];
            },
            GeneralUtility::makeInstance(SiteFinder::class)->getAllSites()
        );
        return array_filter($sites, static fn (array $site): bool => $site !== []);
    }
}
