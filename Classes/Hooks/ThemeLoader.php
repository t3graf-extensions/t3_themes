<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/t3_themes.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\T3Themes\Hooks;

/*
 * This file is part of TYPO3 CMS-based extension "bolt" by b13.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 */

use T3graf\T3Themes\Utility\ThemeHelper;
use TYPO3\CMS\Core\TypoScript\TemplateService;

/**
 * Hooks into the process of building TypoScript templates
 * only works with TYPO3 v8.6.0 natively, otherwise the XCLASS kicks in
 */
class ThemeLoader
{
    /**
     * @var ThemeHelper
     */
    protected $packageHelper;

    public function __construct(ThemeHelper $packageHelper)
    {
        $this->themeHelper = $packageHelper;
    }

    /**
     *  $hookParameters = [
     *      'extensionStaticsProcessed' => &$this->extensionStaticsProcessed,
     *      'isDefaultTypoScriptAdded'  => &$this->isDefaultTypoScriptAdded,
     *      'absoluteRootLine' => &$this->absoluteRootLine,
     *      'rootLine'         => &$this->rootLine,
     *      'startTemplateUid' => $start_template_uid,
     *  ];
     * @param array $hookParameters
     * @param TemplateService $templateService
     */
    public function includeThemeTypoScript(&$hookParameters, TemplateService $templateService)
    {
        // let's copy the rootline value, as $templateService->processTemplate() might reset it
        $rootLine = $hookParameters['rootLine'];
        if (!is_array($rootLine) || empty($rootLine)) {
            return;
        }
        foreach ($rootLine as $level => $pageRecord) {
            $package = $this->themeHelper->getSiteThemePackage((int)$pageRecord['uid']);
            if ($package !== null) {
                $constantsFile = $package->getPackagePath() . 'Configuration/TypoScript/constants.typoscript';
                $setupFile = $package->getPackagePath() . 'Configuration/TypoScript/setup.typoscript';
                if (!file_exists($constantsFile)) {
                    $constantsFile = $package->getPackagePath() . 'Configuration/TypoScript/constants.txt';
                }
                if (!file_exists($setupFile)) {
                    $setupFile = $package->getPackagePath() . 'Configuration/TypoScript/setup.txt';
                }

                if (file_exists($constantsFile)) {
                    $constants = (string)@file_get_contents($constantsFile);
                } else {
                    $constants = '';
                }
                if (file_exists($setupFile)) {
                    $setup = (string)@file_get_contents($setupFile);
                } else {
                    $setup = '';
                }

                // pre-process the lines of the constants and setup and check for "@" syntax
                // @import
                // @sitetitle
                // @clear
                // are the currently allowed syntax (must be on the head of each line)
                $testID = $templateService->config;
                // \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($testID,__LINE__.' in '.__CLASS__);
                $hasRootTemplate = (bool)$templateService->getRootId();
                $RootId = $templateService->getRootId();
                $fakeRow = [
                    'config' => $setup,
                    'constants' => $constants,
                    'nextLevel' => 0,
                    'static_file_mode' => 1,
                    'tstamp' => $setup ? filemtime($setupFile) : time(),
                    'uid' => 'ext_t3_themes_' . (int)$pageRecord['uid'] . $package->getPackageKey(),
                    'title' => $package->getPackageKey(),
                    'root' => $hasRootTemplate
                ];
                $templateService->processTemplate($fakeRow, 'sys_1', (int)$pageRecord['uid'], 'ext_t3_themes_' . $package->getPackageKey());

                if ($hasRootTemplate) {
                    // $templateService->processTemplate() adds the constants and setup info
                    // to the very end however, we like to add ours add root template
                    array_pop($templateService->constants);
                    array_unshift($templateService->constants, $constants);
                    array_pop($templateService->config);
                    array_unshift($templateService->config, $setup);
                    // when having the 'root' flag, set $processTemplate resets the rootline -> we don't want that.
                    $hookParameters['rootLine'] = $rootLine;
                }
            }
        }
    }
}
