# TYPO3 Extension: `TYPO3 Theme Template Management`
[![TYPO3 compatibility](https://img.shields.io/static/v1?label=TYPO3&message=v10.4%20%7C%20v11.5&color=f49800&&logo=typo3&logoColor=f49800)](https://typo3.org)
[![Latest Stable Version](https://img.shields.io/packagist/v/t3graf/t3-themes?label=stable&color=33a2d8)](https://packagist.org/packages/t3graf/t3-themes)
[![Latest Unstable Version](https://img.shields.io/packagist/v/t3graf/t3-themes?include_prereleases&label=unstable)](https://packagist.org/packages/t3graf/t3-themes)
[![Total Downloads](https://img.shields.io/packagist/dt/t3graf/t3-themes?color=1081c1)](https://packagist.org/packages/t3graf/t3-themes)
[![License](https://img.shields.io/packagist/l/t3graf/t3-themes?color=498e7f)](https://gitlab.com/t3graf-extensions/t3_themes/-/blob/master/LICENSE.md)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/t3graf-extensions/t3_themes/master?label=CI%2FCD&logo=gitlab)](https://gitlab.com/t3graf-extensions/t3_themes/pipelines)


> Versatile website template management (themes) for TYPO3.
Change the current theme with just one click, configure in the editor the theme's design and website settings. Template management can be used universally.

### Features

* Change current theme template with one click
* Configure the theme and website settings in the editor

* [Well documented](https://docs.typo3.org/typo3cms/extensions/)

## Installation

### Installation using composer

The recommended way to install the extension is by using [Composer](https://getcomposer.org).

`composer require t3graf/t3_themes`.

### Installation from TYPO3 Extension Repository (TER)

Download and install the extension with the extension manager module.

## Minimal setup

1) Include the static TypoScript of the extension. You don't need to include the TypoScript of Bootstrap package.

<!--## 4. Administration

### Create content element

## 5. Configuration

### Extension settings

### Constants
-->
## Contribute

Please create an issue at [gitlab.com/t3graf-extensions/t3_themes/issues](https://gitlab.com/t3graf-extensions/t3_themes/issues).

**Please use Gitlab only for bug-reports or feature-requests.**

## Credits

This extension was created by Mike Tölle in 2021 for [T3graf media-agentur, Recklinghausen](https://www.t3graf-media.de).

Find examples, use cases and best practices for this extension in our [t3_themes blog series on t3graf.de](https://www.t3graf-media.de/blog/).

[Find more TYPO3 extensions we have developed](https://www.t3graf-media.de/) that help us deliver value in client projects. As part of the way we work, we focus on testing and best practices to ensure long-term performance, reliability, and results in all our code.


## Links

- **Demo:** [www.t3graf-media.de/typo3-extensions/t3_themes](https://www.t3graf-media.de/typo3-extensions/t3_themes)
- **Gitlab Repository:** [gitlab.com/t3graf-extensions/t3_themes](https://gitlab.com/t3graf-extensions/t3_themes)
- **TYPO3 Extension Repository:** [extensions.typo3.org/](https://extensions.typo3.org/)
- **Found an issue?:** [gitlab.com/t3graf-extensions/t3_themes/issues](https://gitlab.com/t3graf-extensions/t3_themes/issues)
