.. every .rst file should include Includes.txt
.. use correct path!

.. include:: Includes.txt

.. Every manual should have a start label for cross-referencing to
.. start page. Do not remove this!

.. _start:

=============================================================
TYPO3 Theme Management
=============================================================

:Version:
   |release|

:Language:
   en

:Authors:
   Development-Team [T3graf media-agentur]

:Email:
   development@t3graf-media.de

:License:
   This extension documentation is published under the
   `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`__ (Creative Commons)
   license

Versatile website template management (themes) for TYPO3.
Change the current theme with just one click, customize the theme&#039;s design. The template management can be used universally. For more information, see the documentation 

**TYPO3**

The content of this document is related to TYPO3 CMS,
a GNU/GPL CMS/Framework available from `typo3.org <https://typo3.org/>`_ .

**Community Documentation**

This documentation is community documentation for the TYPO3 extension TYPO3 Theme Management

It is maintained as part of this third party extension.

If you find an error or something is missing, please:
`Report a Problem <https://github.com/TYPO3-Documentation/TYPO3CMS-Example-ExtensionManual/issues/new>`__

**Extension Manual**

This documentation is for the TYPO3 extension t3_themes.

**For Contributors**

You are welcome to help improve this guide.
Just click on "Edit me on GitHub" on the top right to submit your change request.

.. toctree::
   :maxdepth: 3

   Introduction/Index
   Editor/Index
   Installation/Index
   Configuration/Index
   Developer/Index
   KnownProblems/Index
   ChangeLog/Index
   Sitemap
