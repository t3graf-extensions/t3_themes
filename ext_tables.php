<?php

/*
 * This file is part of the package t3graf/t3_themes.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') || die();

(static function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'T3Themes',
        'themes',
        '',
        'after:web',
        [
        ],
        [
            'access' => 'user,group',
            'icon' => 'EXT:t3_themes/Resources/Public/Icons/modulegroup-theme.svg',
            'labels' => 'LLL:EXT:t3_themes/Resources/Private/Language/locallang_mod_theme.xlf',

        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'T3Themes',
        'themes',
        'selector',
        'top',
        [
            \T3graf\T3Themes\Controller\ThemeController::class => 'listThemes, activateTheme, themeDetails',

        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:t3_themes/Resources/Public/Icons/module-selector.svg',
            'labels' => 'LLL:EXT:t3_themes/Resources/Private/Language/locallang_selector.xlf',
            'navigationComponentId' => 'TYPO3/CMS/Backend/PageTree/PageTreeElement'
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'T3Themes',
        'themes',
        'customizer',
        'after:selector',
        [
            //\T3graf\T3Themes\Controller\ThemeController::class => 'customizeTheme, updateTheme, listThemes',
            \T3graf\T3Themes\Controller\ConfiguratorController::class => 'editConf',

        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:t3_themes/Resources/Public/Icons/module-configurator.svg',
            'labels' => 'LLL:EXT:t3_themes/Resources/Private/Language/locallang_configurator.xlf',
            'navigationComponentId' => 'TYPO3/CMS/Backend/PageTree/PageTreeElement'
        ]
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_t3themes_domain_model_theme', 'EXT:t3_themes/Resources/Private/Language/locallang_csh_tx_t3themes_domain_model_theme.xlf');
    // \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_t3themes_domain_model_theme');
})();
