<?php

/*
 * This file is part of the package t3graf/t3_themes.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

$EM_CONF[$_EXTKEY] = [
    'title' => 'TYPO3 Theme Management',
    'description' => 'Versatile website template management (themes) for TYPO3.
Change the current theme with just one click, customize the theme\'s design. The template management can be used universally. For more information, see the documentation ',
    'category' => 'be',
    'author' => 'Development-Team [T3graf media-agentur]',
    'author_email' => 'development@t3graf-media.de',
    'author_company' => 'T3graf media-agentur UG',
    'state' => 'alpha',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
