<?php

/*
 * This file is part of the package t3graf/t3_themes.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') or die();

// Register autoloading for TypoScript
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['Core/TypoScript/TemplateService']['runThroughTemplatesPostProcessing']['t3_theme'] = T3graf\T3Themes\Hooks\ThemeLoader::class . '->includeThemeTypoScript';
